# lfbag
An untested completely lock-free multi-producer/multi-consumer bounded bag / unordered queue.

[![crates.io page](https://img.shields.io/crates/v/lfbag.svg)](https://crates.io/crates/lfbag/)  
[documentation](https://tormo.gitlab.com/rust-doc/lfbag)

It achieves lockfree-ness by having a slice of `AtomicPtr`s that
can hold one element each, and iterating through this slice.
This means that performance should be inversely proportional to the size / capacity.
The size / capacity can be set when the bag is created, but not changed after that.

Currently it exposes a low-level API (`LFBagPtr`)
and a higher-level safe wrapper (`BoxBag`).

## Optional features:
* `spin_loop_hint`: Use `std::sync::atomic::spin_loop_hint()` which is
stabilized in Rust 1.24 (beta).
* `ptr_nonnull` (WiP): Tries to use [the bleeding edge](https://github.com/rust-lang/rust/pull/46952) `std::ptr::NonNull`.

## License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
