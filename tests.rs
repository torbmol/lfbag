//! Tests should not be run in paralell as many of them spawn threads
//! to try to detect races;
//! Run as `RUST_TEST_THREADS=1 RUST_BACKTRACE=1 cargo test`
//! (or `RUST_BACKTRACE=1 cargo test -- --test-threads 1`).
//! Add `-- --nocapture` for println debugging.

extern crate lfbag;
use lfbag::*;

use std::cell::RefCell;
use std::thread;
use std::fmt::Debug;
use std::str::FromStr;
use std::mem::size_of;
use std::ptr;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::SeqCst;

#[test]
fn deadlock() {
    println!("start deadlock()");
    let rw = BoxBag::<&str>::new(1);
    assert!(rw.pop().is_err());
    let ro = rw.clone_readonly();
    assert!(rw.pop().is_err());
    drop(ro);
    assert!(rw.push("fill").is_ok());
    assert!(rw.push("foo").is_err());
    let wo = rw.clone_writeonly();
    assert!(rw.push("foo").is_err());
    drop(rw);
    assert!(wo.push("foo").is_err());
    let ro: BoxBag<&str,ReadOnly> = BoxBag::new(1).into();
    assert!(ro.pop().is_err());
    println!("end   deadlock()");
}

#[test]
fn drops() {
    println!("start drops()");
    let dropped = RefCell::new(String::new());
    struct PushesOnDrop<'a>(&'a str,&'a RefCell<String>);
    impl<'a> Drop for PushesOnDrop<'a> {
        fn drop(&mut self) {
            self.1.borrow_mut().push_str(self.0);
        }
    }
    let rw = BoxBag::default();
    assert!(rw.try_push(PushesOnDrop("later",&dropped)).is_ok());
    assert!(rw.try_push(PushesOnDrop("dropped",&dropped)).is_ok());
    let clone = rw.clone();
    let first = clone.try_pop();
    drop(rw);
    assert_eq!(&*dropped.borrow(), "");
    assert!(first.is_some());
    drop(clone);
    assert_eq!(&*dropped.borrow(), "dropped");
    assert_eq!(first.unwrap().0, "later");
    println!("end   drops()");
}

static mut DROPPED: usize = 0;
static mut FREED: usize = 0;
#[test]
fn frees() {
    println!("start frees()");
    // use the low-level API to test that it frees too.
    let ptr = LFBagPtr::default();
    ptr.try_push(0b0001).unwrap();
    ptr.try_push(0b0010).unwrap();
    assert_eq!(ptr.try_pop(), Some(1));
    ptr.try_push_freelist(0b0100).unwrap();
    ptr.try_push_freelist(0b1000).unwrap();
    assert_eq!(ptr.try_pop_freelist(), Some(4));
    unsafe {
        DROPPED = 0;
        FREED = 0;
        // the mask makes it possible to detect if a value is freed before it is dropped
        let dropper: fn(usize) = |u| DROPPED |= u & (!DROPPED);
        let freer: fn(usize) = |u| FREED |= u;
        ptr.drop_ref::<ReadWrite>(dropper, freer);
        assert_eq!(DROPPED, 0b0010);
        assert_eq!(FREED, 0b1010);
    }
    println!("end   frees()");
}

#[test]
fn one_pair() {
    println!("start one_pair()");
    let (rx,tx) = BoxBag::<[isize;2]>::default().split();
    let count_to = 1 << 24;
    let rx = thread::spawn(move|| rx.into_iter().collect::<Vec<_>>() );
    for i in 0..count_to as isize {
        tx.push([i,-i]).unwrap();
    }
    drop(tx);
    let mut rx = rx.join().unwrap();
    assert_eq!(rx.len(), count_to);
    rx.sort_by_key(|v| v[0] );
    for (i,got) in (0..count_to as isize).zip(rx) {
        assert_eq!([i,-i], got);
    }
    println!("end   one_pair()");
}

/// use function pointers because closures can never be copied.
/// as long tha parameters are only passed once they should be inlined.
fn onedirectional<T:'static+Send+Debug>
(conumers: usize,  producers: usize,  messages: usize,  into: fn(usize)->T,  from: fn(T)->usize) {
    println!("start onedirectional<{}>({}, {}, {}, ...)",
        size_of::<T>(), conumers, producers, messages
    );
    let bag = BoxBag::<T>::default();
    let pushers = (0..producers).map(|t| {
        let tx = bag.clone_writeonly();
        thread::spawn(move|| {
            // step_by() is unstable, so take a step back to while loops
            let mut i = t;
            while i < messages {
                tx.push(into(i)).unwrap();
                i += producers;
            }
        })
    }).collect::<Vec<thread::JoinHandle<()>>>();
    let poppers = (0..conumers).map(|t| {
        let rx = bag.clone_readonly();
        let to_pop = messages/conumers + if t==0 {messages%conumers} else {0};
        thread::spawn(move|| {
            rx.into_iter().take(to_pop).map(from).collect::<Vec<usize>>()
        })
    }).collect::<Vec<thread::JoinHandle<Vec<usize>>>>();
    for t in pushers {
        t.join().unwrap();
    }
    drop(bag);
    let mut got = vec![0u8; messages];
    for received in poppers.into_iter().flat_map(|t| t.join().unwrap() ) {
        match got[received].checked_add(1) {
            Some(count) => got[received] = count,
            None => panic!("bad test ({})", received),
        }
    }
    let wrong = got.into_iter()
        .enumerate()
        .filter(|&(_,c)| c != 1 )
        .collect::<Vec<(usize,u8)>>();
    assert_eq!(&wrong[..], &[][..]);
    println!("end   onedirectional<{}>({}, {}, {}, ...)",
        size_of::<T>(), conumers, producers, messages
    );
}
// imitate one_pair
//onedirectional(1, 1, 1<<24, |u| [u,!u], |a| a[0] );
#[test]
fn mpmc_tiny() {
    // small type, max instances
    onedirectional(2, 2, 0xffff, |u| u as u16, |u| u as usize );
}
#[test]
fn mpmc_big() {
    // big & slow type, fewer instances
    onedirectional(2, 6, 1000, |u| u.to_string(), |s| usize::from_str(&s).unwrap() );
}
#[test]
fn mpmc_low_alignment() {
    onedirectional(6, 2, 0xffff,
        |u| [(u/256)as u8,u as u8,0u8,0u8, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0],
        |a| a[0] as usize*256 + a[1] as usize
    );
}
#[test]
fn mpsc_zeroable_usize() {
    onedirectional(2, 1, 0xffff, |u| u, |u| u );
}
#[test]
fn spmc_nonzero_usize() {
    onedirectional(1, 2, 0xffff, |u| Box::new((!u,u)), |b| b.1 );
}
static mut ZSTS_POPPED: usize = !0;
#[test]
fn mpmc_zsts() {
    unsafe {
        unsafe fn pops() -> *mut AtomicUsize {
            &mut ZSTS_POPPED as *mut usize as *mut AtomicUsize
        }
        ptr::write(pops(), AtomicUsize::new(0));
        onedirectional(2, 2, 0xff_ff_ff/*16M*/,
            |_| (), |()| (&*pops()).fetch_add(1, SeqCst)
        );
        assert_eq!(AtomicUsize::into_inner(ptr::read(pops())), 0xff_ff_ff_usize);
    }
}