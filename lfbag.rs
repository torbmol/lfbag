/* Copyright 2018 Torbjørn Birch Moltu
 *
 * Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
 * http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
 * http://opensource.org/licenses/MIT>, at your option. This file may not be
 * copied, modified, or distributed except according to those terms.
 */

//! A fully lock-free datastructure that supports concurrent consumption
//! without complicated hazard pointers or epochs:  
//! A bounded bag!

use std::sync::atomic::{AtomicUsize,spin_loop_hint};
use std::sync::atomic::Ordering::{Acquire,Release,Relaxed};
use std::{mem,ptr,slice};
use std::mem::size_of;
use std::thread::yield_now;
use std::process::abort;
use std::marker::PhantomData;
use std::fmt::{self, Debug};

/// To distinguish between read-only, write-only and read-write references
pub trait Counter {
    const READ: bool;
    const WRITE: bool;
    const ONE: usize;
}
// The bag does it's own reference counting, and maintains separate counts of
// the number of references that can read or write (references that can both
// are counted in both)
// To simplify the implementation they are stored in the same AtomicUsize,
// and therefore have only half the number of available bits each.
// Furthermore, the most significant bit for each counter is used as an
// overflow guard: if one was set in the old value after an atomic add,
// the counter might possibly overflow and wrap around, which could possibly
// lead to an use after free exploit. abort() is called if that might happen.
// The maximum reference count is given by 2^(usize_bits/2-1) and is
// 128 for 16-bit architectures and more than enough for anything above that.
#[cfg(target_pointer_width="64")]
const COUNTER_BITS: usize = 31;
#[cfg(target_pointer_width="32")]
const COUNTER_BITS: usize = 15;
#[cfg(target_pointer_width="16")]
const COUNTER_BITS: usize = 7;

pub struct ReadOnly;
impl Counter for ReadOnly {
    const READ: bool = true;
    const WRITE: bool = false;
    const ONE: usize = 1<<(COUNTER_BITS+1); // sets the lsb of the most significant half
}
pub struct WriteOnly;
impl Counter for WriteOnly {
    const READ: bool = false;
    const WRITE: bool = true;
    const ONE: usize = 1;
}
pub struct ReadWrite;
impl Counter for ReadWrite {
    const READ: bool = true;
    const WRITE: bool = true;
    const ONE: usize = ReadOnly::ONE + WriteOnly::ONE;
}
const COUNTER_MSB_MASK: usize = ReadWrite::ONE << COUNTER_BITS;


#[derive(Clone,Copy)]
#[doc(hidden)]
/// Contains the low-level methods for a lock-free reference-counted
/// bounded bag.
///
/// Most of the methods are non-generic to reduce code size.
/// It's a non-RAII `Copy`able pointer to a dynamically sized heap structure.
/// Methods take `self` by value since it's a pointer - only `drop_referece()`
/// consumes.
///
/// The heap structure is:
///
/// ```no_compile
///     capacity: usize,
///     freelist_capacity: usize,
///     counters: AtomicUsize,
///     bag: AtomicUsize...,
///     freelist: AtomicUsize...,
/// ```
///
/// Unused/empty slots are zero, and pushing/popping works by iterating over
/// every slot. This makes performance `O(c)` instead of `O(1)`.
/// It uses `AtomicUsize` instead of `AtomicPtr` for values to be non-generic
/// wich should reduce code size. It might also make optimizations such as
/// storing small nonzero types directly or using tagged pointers easier.  
/// The freelist can be used to store currently unused allocations.  
/// The header fields might suffer somewhat from false sharing if they happen
/// to be on the same cache line as the bag. Putting them after the free list,
/// which I assume gets less frequently updated, might reduce that.
///
/// Erroring out from blocking `push()`es
/// and `pop()`s if there are no other references that can perform the opposite
/// operation works perfectly even for read-write refrences.
/// Deadlocks are still possible if there are multiple read-write references and
/// they're all waiting for the same operation.  
/// The `try_` versions do not check whether the opposite operation can ever
/// be performed.
///
/// `push()` and `pop()` don't do anything more advanced than yielding the
/// thread when they are waiting for a free slot or element to pop,
pub struct LFBagPtr(ptr::NonNull<Header>);

#[repr(C)]
struct Header {
    capacity: usize,
    freelist_capacity: usize,
    counters: AtomicUsize,
    slots: [AtomicUsize; 0]
}
const HEADER_USIZES: usize
    = (size_of::<Header>() + size_of::<usize>() - 1)  /  size_of::<usize>();


/// Number of times to iterate the bag before yielding the thread
const SPINS: u32 = 9;
impl LFBagPtr {
    // simple getters no more complicated than field accesses.

    #[inline]
    pub fn capacity(self) -> usize {
        unsafe{ self.0.as_ref().capacity }
    }
    #[inline]
    pub fn freelist_capacity(self) -> usize {
        unsafe{ self.0.as_ref().freelist_capacity }
    }
    // &self for the lifetime
    #[inline]
    fn counters(&self) -> &AtomicUsize {
        unsafe{ &self.0.as_ref().counters }
    }
    /// returns usizes to let people store small types directly,
    /// but they must not be zero
    #[inline]
    fn slots(&self) -> &[AtomicUsize] {
        unsafe {
            let start = self.0.as_ref().slots[..].as_ptr();
            slice::from_raw_parts(start, self.capacity())
        }
    }
    /// probably don't need a freelist if not storing pointers,
    /// but keep it non-generic
    #[inline]
    fn freelist(&self) -> &[AtomicUsize] {
        unsafe {
            let start = self.0.as_ref().slots[..].as_ptr().offset(self.capacity() as isize);
            slice::from_raw_parts(start, self.freelist_capacity())
        }
    }


    // low-level pushing / popping

    /// pops an element if there are any, and returns the pointer
    fn try_pop_raw(bag: &[AtomicUsize]) -> Option<usize> {
        // Use Acquire just in case, but I think Relexed would be OK
        bag.iter().map(|slot| slot.swap(0, Acquire) ).find(|&e| e != 0 )
    }
    /// Use this variant if only trying once
    fn try_push_raw(bag: &[AtomicUsize],  e: usize) -> Result<(),()> {
        for slot in bag {
            // Release because it must not happen before the content is read
            // if e is a pointer
            if slot.compare_exchange(0, e, Release, Relaxed).is_ok() {
                return Ok(());
            }
        }
        Err(())
    }
    /// Use this variant if spinning
    fn try_push_spin_raw(bag: &[AtomicUsize],  e: usize,  iterations: u32)
    -> Result<(),()> {
        for _ in 0..iterations {
            for slot in bag {
                // Release because it must not happen before the content is read
                // if e is a pointer
                if slot.compare_exchange_weak(0, e, Release, Relaxed).is_ok() {
                    return Ok(());
                }
            }
            spin_loop_hint();
        }
        Err(())
    }

    /// Tries to remove an element from the bag.
    /// The returned element will be non-null.  
    /// This method does not free or reuse the allocation,
    /// call `try_push_freelist()` for that.
    pub fn try_pop(self) -> Option<usize> {
        Self::try_pop_raw(self.slots())
    }
    /// Removes an element from the bag, blocking if necessary.
    /// The returned element will be non-null.  
    /// This method does not free or reuse the allocation,
    /// call `try_push_freelist()` for that.
    ///
    /// # Errors:
    ///
    /// If there are no (other) references that can push, an `Err` is returned.
    pub fn pop(self,  can_push: bool) -> Result<usize,()> {
        let mut spins = SPINS;
        loop {
            if let Some(e) = Self::try_pop_raw(self.slots()) {
                return Ok(e);
            } else if spins != 0 {
                spin_loop_hint();
                spins -= 1;
            } else if !self.have_writers(can_push) {
                return Err(()); // no-one (other than this refernce) can push
            } else {
                spins = SPINS; // reset
                yield_now(); // TODO use CondVar
            }
        }
    }

    /// Tries to store an unused allocation so it can be reused later.  
    /// Returns `Err` if the free list appears full.  
    /// Inserting a zero is a no-op as the value will effectively be
    /// `forget()`en, but safe.
    pub fn try_push_freelist(self,  f: usize) -> Result<(),()> {
        Self::try_push_spin_raw(self.freelist(), f, 2)
    }
    /// Tries to get an existing allocation from the free list.
    /// The returned value will be nonzero.
    pub fn try_pop_freelist(self) -> Option<usize> {
        Self::try_pop_raw(self.freelist())
    }

    /// Tries to store an elemeent in the bag, and
    /// returns `Err` if the bag is full.  
    /// Inserting a zero is a no-op as the value will effectively be
    /// `forget()`en, but safe.
    pub fn try_push(self,  v: usize) -> Result<(),()> {
        Self::try_push_raw(self.slots(), v)
    }
    /// Inserts an element into the bag, and blocks if the bag is full.  
    /// Inserting a zero is a no-op as the value will effectively be
    /// `forget()`en, but safe.
    ///
    /// # Errors:
    ///
    /// If there are no (other) references that can pop, an `Err` is returned.
    pub fn push(self,  v: usize,  can_pop: bool) -> Result<(),()> {
        loop {
            match Self::try_push_spin_raw(self.slots(), v, SPINS) {
                Ok(()) => return Ok(()),
                Err(()) if !self.have_readers(can_pop) => return Err(()),
                Err(()) => yield_now(), // TODO use CondVar
            }
        }
    }

    /// Returns true if there are `int(exclude_self)` references that can write.
    pub fn have_readers(self,  exclude_self: bool) -> bool {
        // I wanted to use Release here, but any false positive should be
        // caught after the next yield_now() iteration.
        let counts = self.counters().load(Relaxed);
        assert_ne!(ReadOnly::ONE, 1);
        let writers = counts >> (COUNTER_BITS+1);
        let min = if exclude_self {1} else {0};
        writers != min
    }
    /// Returns true if there are `int(exclude_self)` references that can read.
    pub fn have_writers(self, exclude_self: bool) -> bool {
        // I wanted to use Release here, but any false positive should be
        // caught after the next yield_now() iteration.
        let counts = self.counters().load(Relaxed);
        assert_eq!(WriteOnly::ONE, 1);
        let readers = counts & ((1<<(COUNTER_BITS+1)) - 1);
        let min = if exclude_self {1} else {0};
        readers != min
    }


    // Reference counting and constructor/destructor

    /// Increase the reference counters by `C::ONE`.
    ///
    /// Will `abort()` if the maximum number of references is exceeded.
    /// This should only be possible on 32+bit platforms if references are
    /// repeatedly cloned and forgotten)
    pub fn clone_ref<C:Counter>(self) {
        let before = self.counters().fetch_add(C::ONE, Acquire);
        if (before & COUNTER_MSB_MASK) != 0 {
            abort();
        }
    }
    /// Decreases the reference counters by `C::ONE` and drops the bag if there
    /// were no other references.
    ///
    /// # Safety:
    ///
    /// must be called more than `clone_ref<C>()s+1` times.
    #[inline]
    pub unsafe fn drop_ref<C:Counter>
    (self,  dropper: unsafe fn(usize),  freer: unsafe fn(usize)) {
        let before = self.counters().fetch_sub(C::ONE, Release);
        if before == C::ONE {
            self.slow_drop(dropper, freer);
        }
    }
    /// Drops remaining elements and frees remaining allocations
    /// and the base Vec.
    /// Takes function pointers to be non-generic,
    /// and does not care about performance.
    #[inline(never)]
    unsafe fn slow_drop(self,  drop: unsafe fn(usize), free: unsafe fn(usize)) {
        while let Some(e) = Self::try_pop_raw(self.slots()) {
            drop(e);
            free(e);
        }
        while let Some(e) = Self::try_pop_raw(self.freelist()) {
            free(e);
        }
        let usizes = HEADER_USIZES + self.capacity() + self.freelist_capacity();
        ptr::drop_in_place(self.0.as_ptr());
        let usize_ptr = self.0.as_ptr() as *mut usize;
        // specify usize to catch errors; heap corruption bugs are weird
        mem::drop(Vec::<usize>::from_raw_parts(usize_ptr, 0, usizes));
    }
    /// Allocates and initializes a new bag.
    ///
    /// * `bag_capacity` sets the maximum number of values the bag can contain.
    /// * `freelist_capacity` sets the capacity of the freelist,
    /// it does not subtract from `bag_capacity`.
    ///
    /// The reference counts are initialized to one read and one write
    /// reference (which can also be one read-write reference).
    pub fn new_with_capacities(bag_capacity: usize,  freelist_capacity: usize)
    -> Self {
        unsafe {
            let usizes = HEADER_USIZES + bag_capacity + freelist_capacity;
            // I assume 0_usize == 0_AtomicUsize == null_AtomicPtr
            let mut v = vec![0_usize; usizes];
            let p = v[..].as_mut_ptr() as *mut Header;
            mem::forget(v);
            ptr::write(p, Header {
                capacity: bag_capacity,
                freelist_capacity: freelist_capacity,
                counters: AtomicUsize::new(ReadWrite::ONE),
                slots: []
            });
            LFBagPtr(ptr::NonNull::new_unchecked(p))
        }
    }


    // niceties

    pub fn ptr_eq(self,  other: Self) -> bool {
        self.0 == other.0
    }
}
impl Default for LFBagPtr {
    /// Allocates and initializes a new bag with a reasonable capacity.
    ///
    /// The reference counts are initialized to one read and one write
    /// reference (which can also be one read-write reference).
    fn default() -> Self {
        Self::new_with_capacities(10, 3)
    }
}
impl Debug for LFBagPtr {
    fn fmt(&self,  fmtr: &mut fmt::Formatter) -> fmt::Result {
        write!(fmtr, "LFBag{{cap: {}}}", self.capacity())
    }
}


/// A wrapper around `LFBagPtr` that stores each element
/// in a `Vec::with_capacity(1)`.
pub struct BoxBag<T, C:Counter=ReadWrite>(
    LFBagPtr,
    /// (In)variance and `Drop`y-ness:
    ///
    /// T: The docs say "May drop one or more T" which is the case
    ///
    /// `*mut T`: Must be invariant over T:
    ///
    /// ```compile_fail
    /// let rw = BoxBag::<&'static str>::default();
    /// rw.push(&String::new("not static"));
    /// ```
    ///
    /// `*const C`: Does not contain any `Counter`, so will not drop any,
    /// and variance doesn't matter
    PhantomData<(T, *mut T, *const C)>,
);
/// elements are never accessed, only moved, so `T:Sync` doesn't matter
unsafe impl<T:Send,C:Counter> Sync for BoxBag<T,C> {}
unsafe impl<T:Send,C:Counter> Send for BoxBag<T,C> {}

fn alloc_vec<T>() -> usize {
    let mut v = Vec::<T>::with_capacity(1);
    let ptr = v.as_mut_ptr();
    mem::forget(v);
    ptr as usize
}
unsafe fn free_vec<T>(fp: usize) {
    mem::drop(Vec::from_raw_parts(fp as *mut T, 0, 1));
}
fn try_pop<T>(inner: LFBagPtr) -> Option<T> {
    unsafe {
        let u = inner.try_pop()?;
        let v = ptr::read(u as *mut T);
        if inner.try_push_freelist(u).is_err() {
            free_vec::<T>(u);
        }
        Some(v)
    }
}
fn pop<T,C:Counter>(this: &BoxBag<T,C>) -> Result<T,()> {
    unsafe {
        let u = this.0.pop(C::WRITE)?;
        let v = ptr::read(u as *mut T);
        if this.0.try_push_freelist(u).is_err() {
            free_vec::<T>(u);
        }
        Ok(v)
    }
}
// could've used macros but would have been less readable and given worse errors
fn try_push<T>(inner: LFBagPtr,  v: T) -> Result<(),T> {
    unsafe {
        let u = inner.try_pop_freelist().unwrap_or_else(alloc_vec::<T>);
        ptr::write(u as *mut T, v);
        inner.try_push(u).map_err(|()| {
            let v = ptr::read(u as *mut T); // get it back
            if inner.try_push_freelist(u).is_err() {
                free_vec::<T>(u)
            }
            v
        })
    }
}
fn push<T,C:Counter>(this: &BoxBag<T,C>,  v: T) -> Result<(),T> {
    unsafe {
        let u = this.0.try_pop_freelist().unwrap_or_else(alloc_vec::<T>);
        ptr::write(u as *mut T, v);
        this.0.push(u, C::READ).map_err(|()| {
            let v = ptr::read(u as *mut T); // get it back
            if this.0.try_push_freelist(u).is_err() {
                free_vec::<T>(u)
            }
            v
        })
    }
}
impl<T,C:Counter> Drop for BoxBag<T,C> {
    #[inline]
    fn drop(&mut self) {
        unsafe {
            let dropper: fn(usize) = |vp| ptr::drop_in_place(vp as *mut T);
            let freer: fn(usize) = |fp| free_vec::<T>(fp);
            self.0.drop_ref::<C>(dropper, freer);
        }
    }
}
impl<T,C:Counter> Clone for BoxBag<T,C> {
    fn clone(&self) -> Self {
        self.0.clone_ref::<C>();
        BoxBag(self.0,PhantomData)
    }
}



  ////////////////////////////////////
 // (mostly) boilerplate from here //
////////////////////////////////////

impl<T> Default for BoxBag<T,ReadWrite> {
    fn default() -> Self {
        BoxBag(LFBagPtr::default(), PhantomData)
    }
}
impl<T> BoxBag<T, ReadWrite> {
    pub fn new(size: usize) -> Self {
        BoxBag(LFBagPtr::new_with_capacities(size, size/3), PhantomData)
    }
    pub fn split(self) -> (BoxBag<T,ReadOnly>, BoxBag<T,WriteOnly>) {
        let inner = self.0;
        mem::forget(self);
        (BoxBag(inner,PhantomData), BoxBag(inner,PhantomData))
    }
    pub fn clone_readonly(&self) -> BoxBag<T, ReadOnly> {
        self.0.clone_ref::<ReadOnly>();
        BoxBag(self.0,PhantomData)
    }
    pub fn clone_writeonly(&self) -> BoxBag<T, WriteOnly> {
        self.0.clone_ref::<WriteOnly>();
        BoxBag(self.0,PhantomData)
    }

    pub fn try_push(&self,  v: T) -> Result<(),T> {
        try_push(self.0, v)
    }
    pub fn push(&self,  v: T) -> Result<(),T> {
        push(self, v)
    }
    pub fn try_pop(&self) -> Option<T> {
        try_pop(self.0)
    }
    pub fn pop(&self) -> Result<T,()> {
        pop(self)
    }
}

impl<T> BoxBag<T, ReadOnly> {
    pub fn try_pop(&self) -> Option<T> {
        try_pop(self.0)
    }
    pub fn pop(&self) -> Result<T,()> {
        pop(self)
    }
    pub fn iter(&self) -> BoxBagIterator<T> {
        BoxBagIterator(self)
    }
}
impl<T> BoxBag<T, WriteOnly> {
    pub fn try_push(&self,  v: T) -> Result<(),T> {
        try_push(self.0, v)
    }
    pub fn push(&self,  v: T) -> Result<(),T> {
        push(self, v)
    }
}
impl<T> From<BoxBag<T, ReadWrite>> for BoxBag<T, ReadOnly> {
    fn from(rw: BoxBag<T, ReadWrite>) -> BoxBag<T, ReadOnly> {
        rw.split().0 // and let the other drop
    }
}
impl<T> From<BoxBag<T,ReadWrite>> for BoxBag<T,WriteOnly> {
    fn from(rw: BoxBag<T,ReadWrite>) -> BoxBag<T,WriteOnly> {
        rw.split().1 // and let the other drop
    }
}
impl<T,C:Counter> Debug for BoxBag<T,C> {
    fn fmt(&self,  fmtr: &mut fmt::Formatter) -> fmt::Result {
        write!(fmtr, "Box{:?}", self.0)
    }
}



/// An iterator that pops elements from until the bag is empty and
/// there are no references that push left.
#[derive(Clone, Debug)]
pub struct BoxBagIntoIterator<T>(BoxBag<T,ReadOnly>);
impl<T> Iterator for BoxBagIntoIterator<T> {
    type Item = T;
    fn next(&mut self) -> Option<T> {
        self.0.pop().ok()
    }
}
impl<T> IntoIterator for BoxBag<T,ReadOnly> {
    type Item = T;
    type IntoIter = BoxBagIntoIterator<T>;
    fn into_iter(self) -> BoxBagIntoIterator<T> {
        BoxBagIntoIterator(self)
    }
}
impl<T> IntoIterator for BoxBag<T,ReadWrite> {
    type Item = T;
    type IntoIter = BoxBagIntoIterator<T>;
    fn into_iter(self) -> BoxBagIntoIterator<T> {
        BoxBagIntoIterator(self.into())
    }
}



/// An iterator that pops elements from a `BoxBag` until the bag is empty
/// and there are no references that can push.
#[derive(Clone, Debug)]
pub struct BoxBagIterator<'a,T:'a>(&'a BoxBag<T,ReadOnly>);
impl<'a,T> Iterator for BoxBagIterator<'a,T> {
    type Item = T;
    fn next(&mut self) -> Option<T> {
        self.0.pop().ok()
    }
}
impl<'a, T> IntoIterator for &'a BoxBag<T,ReadOnly> {
    type Item = T;
    type IntoIter = BoxBagIterator<'a,T>;
    fn into_iter(self) -> BoxBagIterator<'a,T> {
        BoxBagIterator(self)
    }
}